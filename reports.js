'use strict';

const express = require('express');
const multer = require('multer');
const storage = multer.diskStorage({
  destination: 'uploads/',
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname);
  }
});
const upload = multer({ storage: storage });
const moment = require('moment');
const fs = require('fs');
const HttpError = require('standard-http-error');
const models = require('./models');
const geocoding = require('./geocoding');


module.exports = {
	route: function (app) {

		app.post('/report', upload.single('recording'), function (req, res, next) {

			const recording = req.file ? req.file.path : '';
			Promise.resolve().then(function () {

				const userId = req.body.user_id;
				const latitude = req.body.latitude;
				const longitude = req.body.longitude;
				const date = moment(req.body.date);

				if (!userId || !latitude || !longitude || !date || !date.isValid() || !recording.length) {
					throw new HttpError (400, 'Missing/Invalid parameters');
				} else {
					return models.User
						.findById(userId)
						.then (function (user) {
							if (!user) {
								throw new HttpError (400, 'Invalid user');
							}
							return user;
						}).then (function (user) {	
							return geocoding
								.reverseGeocode(latitude, longitude);
						}).then (function (shortAddress) {
							return new models.Report({
								_reportedBy: new models.mongoose.Types.ObjectId(userId),
								'longitude': longitude,
								'latitude': latitude,
								'shortAddress': shortAddress,
								'timestamp': date.valueOf(),
								'recording_path': recording
							}).save();
						}).then(function(report) {
							res.send({ id: report.get('_id') });
						});
				}
			}).catch (function (err) {
				fs.unlink (recording);
				next (err);
			});
		});
	}
}