'use strict';

const models = require('./models');
const moment = require('moment');
const messaging = require('./messaging');

module.exports = function(server) {
	var io = require('socket.io')(server)
		.of('/oboseen')
		.on('connection', function (socket) {
			socket.on('log-in', function(data) {
				if (data.role == 'reviewer') {
					reviewer(socket);
				} else if (data.role == 'reporter') {
					reporter(socket, data.id);
				}
			});
		});
}


function sendReviewResult (review, report) {
	const isValid = report.get('isValid');
	return sendResultToReporter (report.get('_reportedBy'), isValid)
		.then(function () {
			return sendResultToAuthorities(review, report);
		}).then(function() {
			return review;
		});
}

function sendResultToReporter (reporterId, isValid) {
	return models.User
		.findById (reporterId)
		.then(function (user) {
			let message = 'Hi ' + user.get('name') + ', your report has been reviewed. ';
			if (isValid) {
				message += 'The authorities will take the proper actions as soon as possible. \n';		
			} else {
				message += 'Upon review, the authorities did not see an incident requiring further actions. \n'
			}
			message += ' Thank you for your support and keep an eye on the road!';

			return messaging
				.sendSMS('639178445747', message);
		});
}

function sendResultToAuthorities (review, report) {
	return Promise.resolve().then(function () {
		if (report.get('isValid')) {
			let message = 'Incident report\n\n' +
				'Address of incident: ' + report.get('shortAddress') + '\n' +
				'Time of incident: ' + moment(report.get('timestamp')).locale('en').format('hh:mm a') + '\n' + 
				'Summary of incident: ' + review.get('remarks');
			return messaging
				.sendSMS('639165906524', message);
		}
	})
}

function reviewer (socket) {
	socket.join ('reviewer');

	socket.on('validate', function (data, fn) {
		Promise.resolve().then(function() {

			const reportId = data.report_id;
			const remarks = data.remarks;
			const isValid = data.is_valid;
			if (!reportId || !remarks || typeof(isValid) === 'undefined' || typeof(isValid) !== 'boolean') {
				throw new Error ("Missing/Invalid parameters");
			}

			return models.Report
				.findById(new models.mongoose.Types.ObjectId(reportId))
				.then(function (report) {
					if (!report) {
						throw new Error ("Invalid report");
					}
					report.set('isValid', isValid);
					return report.save();
				})
				.then(function(report) {
					return new models.Review({
						'_reportId': new models.mongoose.Types.ObjectId(reportId),
						'remarks': remarks
					}).save().then(function (review) {
						return sendReviewResult (review, report);
					}).then(function (review) {
						return Promise.all([report, review]);
					});
				})
				.spread(function(report, review) {
					return models.User
						.findById(report.get('_reportedBy'))
						.then(function (user) {
							user.set('points', user.get('points') + 20);
							return user.save();
						})
						.then(function(user) {
							socket.to(user.get('_id')).emit('receive points', { user_points: user.get('points') });
							fn ({ review_id: review.get('_id'), report_id: reportId });
						})
				})
			}).catch(function(err) {
				console.log(err);
				fn ({error: err.message});
			})
	})
}

function reporter (socket, userId) {
	socket.join(userId);
	socket.on('submit report', function (data) {

		if (data && data.report_id) {
			models.Report
				.findById(data.report_id)
				.then(function (report) {
					if (report) {
						return Promise.all([
							models.User.findById(report.get('_reportedBy')),
							report]);
					}
				})
				.spread(function (user, report) {
					if (user) {
						const path = report.get('recording_path');
						const recordingFilename = path.substring(path.indexOf('\\') + 1, path.length);

						let data = {
							report_id: report.get('_id'),
							reporter_name: user.get('name'),
							report_date: moment(report.get('timestamp')).format('hh:mm a'),
							report_address: report.get('shortAddress'),
							report_recording_url: 'http://localhost:8000/' + recordingFilename
						};
						socket.to('reviewer').emit('receive report', data);
					}
				})
		}
	})
}