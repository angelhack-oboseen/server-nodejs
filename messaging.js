'use strict';

const request = require('request-promise');
const clientId = '4c6cdf02daee9bc5f2fb2dfe53f0a50f202e1ed7aac19d774feccce7fc483aa5';
const secretKey = '82cdc42432e0b9f92ca014e31a17993519ac8286dc91a238b34300f54c31dd03';
const shortcode = '29290626736';
const randomstring = require('randomstring');

// Chikka api trial only allows 1 test number, use different account for reviewer SMS
const alternateClientId = 'a8da61a7fb5872fa5b55b368b89a1fc557908493668d6362431dafb48a7e7719';
const alternateSecretKey = 'b6a01571917797ff1f64f46a5656b308bfdce76dd5a22ce573d383e0414fea32';
const alternateShortcode = '29290798';

module.exports = {
	sendSMS: function (mobileNumber, message) {
		return Promise.resolve().then(function () {

			let apiClientId = clientId;
			let apiSecret = secretKey;
			let apiShortcode = shortcode;
			if (mobileNumber == '639165906524') {
				apiClientId = alternateClientId;
				apiSecret = alternateSecretKey;
				apiShortcode = alternateShortcode;
			}

			const body = {
				'message_type': 'SEND',
				'mobile_number': mobileNumber,
				'shortcode': apiShortcode,
				'client_id': apiClientId,
				'secret_key': apiSecret,
				'message_id': randomstring.generate(32),
				'message': message
			};

			return request.post('https://post.chikka.com/smsapi/request', { form: body })
				.then(function (body) {
					console.log(body);
				});
		});
	}
}