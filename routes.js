'use strict';

const express = require('express');
const router = express.Router();
const reports = require('./reports');
const models = require('./models');
const HttpError = require('standard-http-error');

module.exports = {
	route: function (app) {
		router.post('/register', function (req, res, next) {
			Promise.resolve().then(function () {

				const firstName = req.body.first_name;
				const lastName = req.body.last_name;
				const email = req.body.email;
				const mobileNumber = req.body.mobile_number;

				console.log(firstName);
				console.log(lastName);
				console.log(email);
				console.log(mobileNumber);

				if (!firstName || !lastName || !email || !mobileNumber || mobileNumber.length > 10 || mobileNumber.length < 10) {
					throw new HttpError(400, "Missing parameters" );
				} else {
					return new models.User({
						'name': firstName + ' ' + lastName,
						'email': email,
						'mobileNumber': '63' + mobileNumber
					})
					.save()
					.then (function (user) {
						res.send({ id: user.get('_id')});
					})
					.catch (function (err) {
						console.log(err);
						throw new HttpError(500, err.message);
					});
				}
			}).catch(next);
		});

		app.use('/', router);
		reports.route (app);
	}
}