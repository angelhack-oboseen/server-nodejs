'use strict';

// use bluebird for promises
global.Promise = require('bluebird');

var express = require('express');
var app = exports.app = express();

const httpServer = require('http')
	.createServer(app);

const morgan = require('morgan');
const bodyParser = require('body-parser');


app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('uploads'));
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept-Type');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
})

//set up routes
require('./routes').route(app);

app.use(function(err, req, res, next) {
	console.error(err.stack);
	var status = 500;
	var message = 'An unknown error occurred';
	if (err.code) {
		status = err.code;
	}
	if (err.message) {
		message = err.message;
	}
	res.status(status).json({message: message});
});


require('./sockets')(httpServer);

httpServer.listen(8000, '0.0.0.0', function() {
	console.log('listening on port 8000');
});


