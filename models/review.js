'use strict';

module.exports = function (mongoose) {
	var ReviewSchema = new mongoose.Schema({
		_reportId: mongoose.Schema.Types.ObjectId,
		remarks: String
	});

	var name = 'Review';

	return {
		name: name,
		mongooseModel: mongoose.model(name, ReviewSchema)
	};
}