'use strict';

var fs        = require('fs');
var path      = require('path');
var basename  = path.basename(module.filename);
var db        = {};

const mongoose = require('mongoose').set('debug', true);
mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/oboseen');

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
    var model = require(path.join(__dirname, file))(mongoose);
    db[model.name] = model.mongooseModel;
  });

db.mongoose = mongoose;

module.exports = db;
