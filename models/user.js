'use strict';

module.exports = function (mongoose) {
	var UserSchema = new mongoose.Schema({
		name: String,
		email: String,
		mobileNumber: String,
		points: { type: Number, default: 0 }
	});

	const name = 'User';

	return {
		name: name,
		mongooseModel: mongoose.model(name, UserSchema)
	};
}