'use strict';

module.exports = function (mongoose) {
	var ReportSchema = new mongoose.Schema({
		_reportedBy: mongoose.Schema.Types.ObjectId,
		timestamp: Number,
		latitude: Number,
		longitude: Number,
		shortAddress: String,
		recording_path: String,
		isValid: { type: Boolean, default: null }
	});

	var name = 'Report';

	return {
		name: name,
		mongooseModel: mongoose.model(name, ReportSchema)
	};
}