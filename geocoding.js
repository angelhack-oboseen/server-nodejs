'use strict';

const apiKey = 'AIzaSyAguUK3ql2WdI7yGhII_60Y5RyaM4jwLzg';
const request = require('request-promise');

module.exports = {
	reverseGeocode: function (latitude, longitude) {
		return Promise.resolve().then(function() {
			const options = {
				qs: {
					latlng: latitude + ',' + longitude,
					result_type: 'street_address|intersection',
					key: apiKey
				},
				json: true
			};
			return request('https://maps.googleapis.com/maps/api/geocode/json', options)
				.then(function(result) {
					let shortAddress = 'Not available';
					if (result.status == 'OK') {
						shortAddress = '';
						const addressComponents = result.results[0].address_components;
						for (let i = 0, len = addressComponents.length; i < len; i++) {
							console.log(addressComponents[i].types);
							if (addressComponents[i].types.includes('street_number') || 
								addressComponents[i].types.includes('sublocality') || 
								addressComponents[i].types.includes('locality')) {
								shortAddress += addressComponents[i].long_name + ' ';
							}
						}
					}
					return shortAddress.trim();
				});
		})
	}
}